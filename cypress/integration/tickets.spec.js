describe("Tickets", () => {
    beforeEach(() => cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html"));
    it("fills all the text input fields", () => {
        const firstName = "Jamaira";
        const lastName = "Dias";
        const fullName = "Jamaira Dias";

        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("jamairadias@teste.com.br");
        cy.get("#requests").type("Vegetarian");
        cy.get("#signature").type(fullName);
    });

    it("select two tickets", () => {
        cy.get("#ticket-quantity").select("2");
    });

    it("select 'vip' tickets type", () => {
        cy.get("#vip").check();
    });

    it("selects 'friends' and 'publication', then uncheck 'friend'", () => {
        cy.get("#social-media").check();
    });

    it("selects 'I Agree' checkbox", () => {
        cy.get("#agree").check();
    });
    
    it("has 'TICKETBOX' header's heading", () => {
        cy.get("header h1").should("contain", "TICKETBOX");
    });

    it("alerts on invalid email", () => {
        cy.get("#email").type("jamairadiasteste-gmail.com");
        cy.get("#email.invalid").should("exist");    
    });

    it("fills and reset the form", () => {
        const firstName = "Jamaira";
        const lastName = "Dias";
        const fullName = "Jamaira Dias";

        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("jamairadias@teste.com.br");
        cy.get("#ticket-quantity").select("2");
        cy.get("#vip").check();
        cy.get("#friend").check();
        cy.get("#requests").type("IPA beer");

        cy.get(".agreement p").should(
            "contain", 
            'I, Jamaira Dias, wish to buy 2 VIP tickets.'
        );

        cy.get("#agree").check();
        cy.get("#signature").type(fullName);
        cy.get("button[type='submit']")
          .as("submitButton")
          .should("not.be.disabled");
        
        cy.get("button[type='reset']").click();
        
        cy.get("@submitButton").should("be.disabled");
          
    });

    it("fills mandatory fields using support command", () => {
        const customer = {
            firstName: "Jamaira",
            lastName: "Dias",
            email: "jamairadias@teste.com.br"
        };

        //função criada em commands.js
        cy.fillMandatoryFields(customer);

        cy.get("button[type='submit']")
          .as("submitButton")
          .should("not.be.disabled");

        cy.get("#agree").uncheck();

        cy.get("@submitButton").should("be.disabled");

    });
});